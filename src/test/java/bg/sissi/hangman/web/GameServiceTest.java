package bg.sissi.hangman.web;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.is;
import bg.sissi.hangman.model.Game;
import org.hamcrest.Matchers;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mockito;
import org.mockito.Spy;
import org.mockito.junit.jupiter.MockitoExtension;

@ExtendWith(MockitoExtension.class)
public class GameServiceTest {

  @Spy GameService gameServ = new GameService();

  @Test
  public void testGameCreation() {
    GameService gameService = new GameService();
    Game game = gameService.startNewGame();
    assertThat(game, is(gameService.getGameById(game.getId())));
  }

  @Test
  public void whenLetterIsNotPresent_thenLifesAreDecreased() {
    Game gameDummy = new Game("tableee");
    Mockito.when(gameServ.startNewGame()).thenReturn(gameDummy);
    Mockito.when(gameServ.getGameById(Mockito.anyString()))
        .thenReturn(gameDummy, gameDummy, new Game("tiger"));
    Game game = gameServ.startNewGame();

    assertThat(game.getLifes(), is(8));

    gameServ.checkLetter(game.getId(), "z");

    assertThat(game.getLifes(), is(7));
    assertThat(game.getLetters(), Matchers.contains('z'));

    gameServ.checkLetter(game.getId(), "d");
    assertThat(game.getLifes(), is(6));

    gameServ.checkLetter(game.getId(), "a");

    assertThat(game.getLifes(), is(6));

    gameServ.checkLetter(game.getId(), "z");
    assertThat(game.getLifes(), is(6));
    assertThat(game.getLetters().contains('z'), Matchers.is(true));
  }

  @Test
  public void testIfGameExists() {
    Game newGame = new Game("lion");
    Mockito.when(gameServ.getGameById("1")).thenReturn(newGame);
    assertThat(gameServ.getGameById("1"), Matchers.notNullValue());
    assertThat(gameServ.getGameById("3"), Matchers.nullValue());
    assertThat(gameServ.getGameById("1"), is(newGame));
  }
}
