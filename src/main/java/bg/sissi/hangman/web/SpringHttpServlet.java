package bg.sissi.hangman.web;

import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import org.springframework.context.ApplicationContext;

public abstract class SpringHttpServlet extends HttpServlet {

  /** */
  private static final long serialVersionUID = -5751716011242010873L;

  @Override
  public void init(ServletConfig config) throws ServletException {
    ((ApplicationContext) config.getServletContext().getAttribute("springContext"))
        .getAutowireCapableBeanFactory()
        .autowireBean(this);
  }
}
