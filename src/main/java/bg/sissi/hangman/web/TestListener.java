package bg.sissi.hangman.web;

import java.util.stream.IntStream;
import javax.servlet.ServletContextEvent;
import javax.servlet.ServletContextListener;
import javax.servlet.annotation.WebListener;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;

@WebListener
public class TestListener implements ServletContextListener {

  private static final String[] ALPHABET =
      IntStream.range('a', 'z' + 1)
          .mapToObj(c -> Character.toString((char) c))
          .toArray(String[]::new);

  @Override
  public void contextInitialized(ServletContextEvent sce) {
    AnnotationConfigApplicationContext context =
        new AnnotationConfigApplicationContext("bg.sissi.hangman.web");
    sce.getServletContext().setAttribute("springContext", context);
    sce.getServletContext().setAttribute("alphabet", ALPHABET);
  }

  @Override
  public void contextDestroyed(ServletContextEvent sce) {
    // TODO Auto-generated method stub

  }
}
