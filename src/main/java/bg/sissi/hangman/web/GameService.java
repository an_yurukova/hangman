package bg.sissi.hangman.web;

import bg.sissi.hangman.model.Game;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;
import org.springframework.stereotype.Component;

@Component
public class GameService {
  public List<Game> games = new ArrayList<Game>();

  private static final String[] WORDS = {"account", "absolute", "equal", "access", "escape"};

  public Game startNewGame() {

    Random random = new Random();

    Game game = new Game(WORDS[random.nextInt(WORDS.length)]);
    games.add(game);
    return game;
  }

  public Game getGameById(String id) {

    for (Game game : games) {
      if (game.getId().equals(id)) {
        return game;
      }
    }
    return null;
  }

  public void checkLetter(String id, String letter) {
    Game game = getGameById(id);
    game.getLetters().add(letter.charAt(0));
    if (!game.getWord().contains(letter)) {

      game.setLifes(game.getLifes() - 1);
    }
  }
}
