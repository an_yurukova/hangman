package bg.sissi.hangman.web;

import bg.sissi.hangman.model.Game;
import java.io.IOException;
import java.util.stream.Collectors;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.springframework.beans.factory.annotation.Autowired;

@WebServlet("/games")
public class StartNewGameServlet extends SpringHttpServlet {

  /** */
  private static final long serialVersionUID = 4934025118997129926L;

  @Autowired GameService gameService;

  @Override
  protected void doGet(HttpServletRequest req, HttpServletResponse resp)
      throws ServletException, IOException {
    Game game = getGame(req);
    req.setAttribute("game", game);

    String letters =
        game.getLetters().stream().map(c -> c.toString()).collect(Collectors.joining(""));
    if (letters.isEmpty()) {
      req.setAttribute("word", game.getWord().replaceAll("[a-z]", "_"));
    } else {

      req.setAttribute("word", game.getWord().replaceAll("[^" + letters + "]", "_"));
    }

    if (!((String) req.getAttribute("word")).contains("_")) {
      req.setAttribute("message", "Well done");
      req.getRequestDispatcher("/game-over.jsp").forward(req, resp);
    } else if (game.getLifes() == 0) {
      req.setAttribute("message", "Sorry");
      req.getRequestDispatcher("/game-over.jsp").forward(req, resp);
    } else {
      req.getRequestDispatcher("/display-game.jsp").forward(req, resp);
    }
  }

  @Override
  protected void doPost(HttpServletRequest req, HttpServletResponse resp)
      throws IOException, ServletException {

    System.out.println("Starting new game...");
    String gameId = gameService.startNewGame().getId();
    resp.sendRedirect("/games?id=" + gameId);
  }

  private Game getGame(HttpServletRequest req) {
    String gameId = req.getParameter("id");
    return gameService.getGameById(gameId);
  }
}
