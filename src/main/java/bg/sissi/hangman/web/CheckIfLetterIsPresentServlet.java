package bg.sissi.hangman.web;

import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.springframework.beans.factory.annotation.Autowired;

@WebServlet("/games/check")
public class CheckIfLetterIsPresentServlet extends SpringHttpServlet {
  @Autowired GameService gameService;

  /** */
  private static final long serialVersionUID = 1L;

  @Override
  protected void doPost(HttpServletRequest req, HttpServletResponse resp)
      throws ServletException, IOException {
    String letter = req.getParameter("letter");
    String id = req.getParameter("id");

    gameService.checkLetter(id, letter);
    resp.sendRedirect("/games?id=" + id);
  }
}
