package bg.sissi.hangman.model;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

public class Game {

  private String id;
  private int lifes;
  private String word;
  private List<Character> letters;

  public Game(String word) {
    this.id = UUID.randomUUID().toString();
    this.lifes = 8;
    this.word = word;
    this.letters = new ArrayList<Character>();
  }

  public String getId() {
    return id;
  }

  public void setId(String id) {
    this.id = id;
  }

  public int getLifes() {
    return lifes;
  }

  public void setLifes(int countLifes) {
    this.lifes = countLifes;
  }

  public List<Character> getLetters() {
    return letters;
  }

  public void setLetters(List<Character> letters) {
    this.letters = letters;
  }

  public String getWord() {
    return word;
  }

  public void setWord(String word) {
    this.word = word;
  }
}
