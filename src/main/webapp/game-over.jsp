<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1" isELIgnored="false"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>Game over</title>
</head>
<body>

	<div style="text-align: center; background-color: #ffff99;">${message}</div>
	
	<form method="POST" action="/games">

		<input type="submit" value="Start new game"
			style="border-radius: 3px; background-color: #0080ff;" />

	</form>

</body>
</html>