<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1" isELIgnored="false"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>Hangman</title>
</head>
<body>
	<div
		style="padding: 1%; background-color: #000080; color: white; font-weight: bold; font-family: verdana;">Hangman</div>

	<div style="padding: 1%; background-color: #ffd480;">
		<div style="padding-left: 90%">Tries left: ${game.lifes}</div>
		<div
			style="margin-bottom: 1.5%; margin-top: 3%; font-weight: bold; font-family: verdana;">${word}</div>


		<div style="width: 300px;"></div>

		<c:forEach items="${applicationScope['alphabet']}" var="letter">
			<form method="POST" action="/games/check" style="display: inline;">
				<input name="id" value="${game.id}" style="display: none;">
				<input type="submit" name="letter" value="${letter}"
					${fn:contains ((game.letters), letter) ? "disabled" : ""}
					style="border-radius: 3px; font-weight: bold; background-color: #00a3cc"></input>
			</form>
		</c:forEach>



	</div>



</body>